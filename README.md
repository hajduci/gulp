# README #
Gulp base with popular modules. Node modules need to be installed manually.

### Summary ###

1. Setup
2. Modules
3. Contributions
4. References
5. Contact

### 1. Setup ###

1. Install Node JS on local machine - [Node.JS](https://nodejs.org/en/)
2. Create a project directory
3. Setup Gulp in project directory - [Gulp](https://gulpjs.com/)
4. Test Gulp: Create a gulp task in gulpfile.js like gulp.task('echo', function(){ console.log('Hello World');}); and run "gulp echo" in terminal from the project directory where gulpfile.js resides.
5. Create subdirectories in project directory - Dev and Production (you can name directories however you like, just be sure to update them in code)
6. Install Node modules listed in 2. Modules
7. Run default task to see if there are errors

### 2. Modules ###

Modules which are included in this repo are:

* [gulp-sass](https://www.npmjs.com/package/gulp-sass) - SASS compiler
* [browser-sync](https://browsersync.io/docs/gulp) - Local server and Browser Synchronization
* [gulp-useref](https://www.npmjs.com/package/gulp-useref) - File concatination (Used for CSS and JS)
* [gulp-uglify](https://www.npmjs.com/package/gulp-uglify) - JS Minify
* [gulp-if](https://github.com/robrich/gulp-if) - Condition control
* [gulp-cssnano](https://www.npmjs.com/package/gulp-cssnano) - CSS Minify
* readline-sync(https://github.com/anseki/readline-sync) - Read input from terminal

### Contribution guidelines ###

* Create a branch from Master
* Contact for code review
* Pull request
* Merge

### References ###
[CSS Tricks article](https://css-tricks.com/gulp-for-beginners/)

### Contact ###

[marquiseniw@gmail.com](mailto:marquiseniw@gmail.com)